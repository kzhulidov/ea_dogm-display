#include <SPI.h>

#define outCSB 6
#define outRS 7

SPISettings settings(1000000, MSBFIRST, SPI_MODE3);

void transferByte(byte data, bool rsLow = true) {
  SPI.beginTransaction(settings);

  digitalWrite(outCSB, LOW);
  digitalWrite(outRS, rsLow ? LOW : HIGH);
  delay(1);

  SPI.transfer(data);
  
  digitalWrite(outCSB, HIGH);
  
  SPI.endTransaction();
}

void initLcd() {
  transferByte(0);
  transferByte(0b00111001);
  delay(10);
  transferByte(0b00011100);
  transferByte(0b01010010);
  transferByte(0b01101001);
  transferByte(0b01110100);
  transferByte(0b00111000);
  delay(10);
  transferByte(0b00001100);
  transferByte(0b00000001);
  transferByte(0b00000110);
}

void setLcdPos(byte line, byte pos) {
  transferByte(0b10000000 + line * 0x40 + pos);
}

void writeLcdString(byte line, byte pos, const char* str, int len) {
  setLcdPos(line, pos);
  for (int i = 0; i < len; i++) {
    transferByte(str[i], false);
  }
}

void setup() {
  // put your setup code here, to run once:
  pinMode(outCSB, OUTPUT);
  pinMode(outRS, OUTPUT);
  SPI.begin();

  delay(50);
  initLcd();

  writeLcdString(0, 0, "millis:", 7);
}

void loop() {
  // put your main code here, to run repeatedly:
  char lcdStr[16] = {0};
  writeLcdString(1, 0, ltoa(millis(), lcdStr, 10), 16);
  delay(300);
}
